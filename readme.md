# Figures

This project generates figures in html. You can specify the element where you want to draw the figure, coordinates, size and color.

## Install project

- Install [Node.js](https://nodejs.org/en/)
- Run `npm install` in command line

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:8080/`. The app will automatically reload if you change any of the source files.


