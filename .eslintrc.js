module.exports = {
  env: {
    browser: true,
    es6: false,
  },
  extends: 'airbnb',
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 5,
    sourceType: 'module',
  },
  rules: {
    "linebreak-style": 0,
    "semi": [2, "always"]
  },
};
