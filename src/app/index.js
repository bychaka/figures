import '../assets/main.scss';
var _ = require('lodash');

function Figure(container, x, y, color) {
  this.container = container;
  this.x = x;
  this.y = y;
  this.color = color;
}

Figure.prototype.draw = function(){
  var figure = document.createElement('div');
  figure.className = 'figure';
  figure.style.left = `${this.x}px`;
  figure.style.top = `${this.y}px`;
  figure.style.backgroundColor = this.color;
  document.querySelector(`.${this.container}`).appendChild(figure);
};

function Square(container, x, y, color, size) {
  Figure.apply(this, arguments);
  this.size = size;
}

Square.prototype.draw = function() {
  var square;
  Figure.prototype.draw.apply(this, arguments);
  square = _.first(document.querySelectorAll(`.${this.container} > .figure`));
  square.classList.add('square');
  square.style.height = `${this.size}px`;
  square.style.width = `${this.size}px`;
};

function Triangle(container, x, y, color, size) {
  Figure.apply(this, arguments);
  this.size = size;
}

Triangle.prototype.draw = function() {
  var triangle;
  Figure.prototype.draw.apply(this, arguments);
  triangle = _.first(document.querySelectorAll(`.${this.container} > .figure`));
  triangle.classList.add('triangle');
  triangle.style.borderWidth = `${this.size}px`;
  triangle.style.top = `${(this.y)-(this.size)}px`;
  triangle.style.backgroundColor = 'transparent';
  triangle.style.borderBottomColor = `${this.color}`;
};

window.onload = function() {
  var square = new Figure('main', 175, 60, '#f00');
  square.draw();
  var quadro = new Square('secondary', 250, 77, '#ffc933', 100);
  quadro.draw();
  var trio = new Triangle('third', 300, 10, '#3683ff', 80);
  trio.draw();
};
