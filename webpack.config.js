const LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
  entry: './src/app/index.js',
  output: {
    filename: 'bundle.js',
  },
  devServer: {
    index: 'index.html',
    watchContentBase: true,
  },
  watch: true,
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [{
          loader: 'style-loader',
        }, {
          loader: 'css-loader',
        }, {
          loader: 'sass-loader',
          options: {
            includePaths: [],
          },
        }],
      }],
  },
  plugins: [
    new LiveReloadPlugin({}),
  ],
};
